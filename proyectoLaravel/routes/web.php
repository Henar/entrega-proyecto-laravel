<?php

use App\Http\Controllers\EstanciaController;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\ReservaController;
use App\Http\Controllers\ViajeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/', [InicioController::class, "__invoke"])->name('inicio.inicio');

//Viajes-Destinos
Route::get('viajes', [ViajeController::class, 'index'])->name('viajes.index');

Route::post('viajes', [ViajeController::class, 'store'])->name("viajes.store");

Route::get('viajes/create', [ViajeController::class, 'create'])
->middleware('auth')
->name('viajes.create');

Route::get('viajes/{viaje}', [ViajeController::class, 'show'])->name('viajes.show');

Route::put('viajes/{viaje}', [ViajeController::class, 'update'])
->middleware('auth')
->name("viajes.update");

Route::get('viajes/{viaje}/editar', [ViajeController::class, 'edit'])
->middleware('auth')
->name('viajes.edit');

Route::delete('viajes/{viaje}', [ViajeController::class, 'destroy'])
->middleware('auth')
->name("viajes.destroy");

//Trait
Route::resource('viajes',   ViajeController::class);


//Reservas
Route::get('reservas', [ReservaController::class, 'index'])->name('reservas.index');

Route::post('reservas', [ReservaController::class, 'store'])->name("reservas.store");

Route::get('reservas/crear/{lugar}', [ReservaController::class, 'create'])
->middleware('auth')
->name('reservas.create');

Route::get('reservas/{reserva}', [ReservaController::class, 'show'])->name('reservas.show');

Route::put('reservas/{reserva}', [ReservaController::class, 'update'])
->middleware('auth')
->name("reservas.update");

Route::get('reservas/{reserva}/editar', [ReservaController::class, 'edit'])
->middleware('auth')
->name('reservas.edit');

Route::delete('reservas/{reserva}', [ReservaController::class, 'destroy'])
->middleware('auth')
->name("reservas.destroy");


//Estancias
Route::get('estancias', [EstanciaController::class, 'index'])->name('estancias.index');

Route::get('estancias/{estancia}', [EstanciaController::class, 'show'])->name('estancias.show');

Route::delete('estancias/{estancia}', [EstanciaController::class, 'destroy'])
->middleware('auth')
->name("estancias.destroy");




//JetStream
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
