<?php

namespace Database\Factories;

use App\Models\Estancia;
use App\Models\Lugar;
use App\Models\Model;
use App\Models\Reserva;
use App\Models\Transporte;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReservaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Reserva::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $fReserva=$this->faker->date($format = 'Y-m-d', $min = 'now');

        return [
            //'origenLugar_id'=>Lugar::all()->random()->id,
            'lugar_id'=>Lugar::all()->random()->id,
            'transporte_id'=>Transporte::all()->random()->id,
            'cliente_id'=>User::all()->random()->id,
            'estancia_id'=>Estancia::all()->random()->id,
            'fechaReserva'=>$fReserva,
            'fechaFinReserva'=>$this->faker->date($format = 'Y-m-d', $min = $fReserva),


        ];
    }
}
