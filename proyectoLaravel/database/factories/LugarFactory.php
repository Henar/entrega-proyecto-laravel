<?php

namespace Database\Factories;

use App\Models\Lugar;
use App\Models\Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class LugarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Lugar::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $dir='public/assets/imagenes';
        $width='400';
        $height='350';
        $ciudad=$this->faker->unique()->city;

        return [
            'pais'=>$this->faker->country,
            'ciudad'=>$ciudad,
            'descripcion'=>$this->faker->realText($maxNbChars = 300, $indexSize = 2),
            //(El false es para que no ponga la ruta antes del nombre de la imagen)
            //El null es el texto que le da, es como una marca de agua
            'imagen'=>$this->faker->image($dir, $width, $height, null, false), 
            'slug'=>Str::slug($ciudad, '-'),
            'precio'=>$this->faker->randomFloat($nbMaxDecimals = 2, $min = 15, $max = 1000),
            
        ];
    }
}
