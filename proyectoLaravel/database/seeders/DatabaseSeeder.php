<?php

namespace Database\Seeders;

use App\Models\Estancia;
use App\Models\Lugar;
use App\Models\Reserva;
use App\Models\Transporte;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->delete();
        $this->call(UserSeeder::class);
        \App\Models\User::factory(15)->create();
         
         Estancia::factory(25)->create();
         
         Lugar::factory(30)->create();
         
         DB::table('transportes')->delete();
         $this->call(TransporteSeeder::class);
         
         Reserva::factory(10)->create();
    }
}
