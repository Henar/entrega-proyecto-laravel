<?php

namespace Database\Seeders;

use App\Models\Transporte;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class TransporteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    protected $vehiculos=array('Avión', 'Barco', 'Tren', 'Coche','Autobus', 'Taxi', 'Avioneta', 'Bicicleta', 'Caravana', 'Moto');
 

    public function run()
    {
        foreach ($this->vehiculos as $vehiculo){
            $transpor = new Transporte();
            $transpor->nombre = $vehiculo;
            $transpor->slug = Str::slug($vehiculo, '-'); 
            $transpor->save();
        }
        $this->command->info('Tabla transportes inicializada con datos');
        //Arr::random($vehiculos);
    }
}
