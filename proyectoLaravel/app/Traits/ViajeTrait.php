<?php

namespace App\Traits;

use App\Models\Lugar;

trait ViajeTrait {
    public function index() {
        // Fetch all the users from the 'users' table.
        $viajes = Lugar::all();
        return view('reservas.create')->with(compact('viajes'));
    }
}
?>