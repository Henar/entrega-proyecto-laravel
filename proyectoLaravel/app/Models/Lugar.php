<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lugar extends Model
{
    use HasFactory;

    protected $table='lugares';

    protected $fillable = [
        'pais',
        'ciudad',
        'precio',
        'imagen',
        'descripcion'
    ];

    public function reservas(){
        return $this->hasMany(Reserva::class);
    }
    
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
