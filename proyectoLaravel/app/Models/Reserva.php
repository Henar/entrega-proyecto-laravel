<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    use HasFactory;

    protected $table='reservas';

    protected $fillable = [
        'cliente_id',
        'estancia_id',
        'fechaReserva',
        'fechaFinReserva',
        'transporte_id',
        'lugar_id'
    ];

    public function estancia(){
        return $this->belongsTo(Estancia::class);
    }

    public function lugar(){
        return $this->belongsTo(Lugar::class);
    }

    public function transporte(){
        return $this->belongsTo(Transporte::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    
    // public function getRouteKeyName()
    // {
    //     return 'slug';
    // }
}
