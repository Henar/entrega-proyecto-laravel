<?php

namespace App\Http\Controllers;

use App\Models\Lugar;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Traits\ViajeTrait;

class ViajeController extends Controller
{

    use ViajeTrait;
    
    public function index(){
        //$viajes=Lugar::all();
        $viajes=Lugar::paginate(9);
		return view('viajes.index', compact('viajes'));
    }

    public function show(Lugar $viaje){
		$v=Lugar::findOrFail($viaje->id);
		return view('viajes.show', ["viaje"=>$v]);
    }

    public function create(){
        return view('viajes.create');
    }

    public function edit(Lugar $viaje){
       $v=Lugar::findOrFail($viaje->id);
	   return view('viajes.edit', ["viaje"=>$v]);
    }


    public function store (Request $request){
        
       //return $request->all();
        

        $via=new Lugar();
        // $datos=$request->all();
        // $datos['slug'] = Str::slug($request->ciudad, '-');
        // $datos['imagen']=$request->imagen->store('', 'viajes');
        // $via=Lugar::create($datos);
        $via->ciudad=$request->ciudad;
        $via->pais=$request->pais;
        $via->precio=$request->precio;
        $via->descripcion=$request->descripcion;
        $via->slug=Str::slug($request->ciudad, '-');
        $via->imagen=$request->imagen->store('', 'viajes');
        $via->save();
        // $via=Lugar::create($datos);
        return redirect()->route('viajes.show', $via)->with('mensaje', "Viaje actualizado con éxito");

    }
    
    public function update(Request $request, Lugar $viaje){

        $request->validate([
            'pais'=>'required|max:255|min:2',
            'ciudad'=>'required|max:255|min:2',
            'precio'=>'required|numeric|min:1|max:999999',
            'imagen'=>'nullable'
        ]);

        $viaje->pais=$request->pais;
        $viaje->ciudad=$request->ciudad;
        $viaje->slug=Str::slug($request->ciudad, '-');
        $viaje->precio=$request->precio;
        $viaje->descripcion=$request->descripcion;
        if(!empty($request->imagen) && $request->imagen->isValid()){
            Storage::disk('viajes')->delete($viaje->imagen);
            $viaje->imagen=$request->imagen->store('', 'viajes');
        }
       
        $viaje->save();

        return redirect()->route('viajes.show', $viaje)->with('mensaje', "Viaje actualizado con éxito");

    }

    public function destroy(Lugar $viaje){
        $viaje->delete();
        return redirect()->route('viajes.index')->with('mensaje', 'Destino eliminado correctamente.');
    }
}
