<?php

namespace App\Http\Controllers;

use App\Models\Estancia;
use Illuminate\Http\Request;

class EstanciaController extends Controller
{
    public function index(){
        $estancias=Estancia::all();
        $estancias=Estancia::paginate(9);
		return view('estancias.index', compact('estancias'));
    }

    public function show(Estancia $estancia){
        
        $es=Estancia::findOrFail($estancia->id);
        return view('estancias.show', ["estancia"=>$es]);
    }


    public function destroy(Estancia $estancia){
        $estancia->delete();
        return redirect()->route('estancias.index')->with('mensaje', 'Estancia eliminada correctamente.');
    }
}