<?php

namespace App\Http\Controllers;

use App\Models\Estancia;
use App\Models\Lugar;
use App\Models\Reserva;
use App\Models\Transporte;
use Illuminate\Http\Request;
use App\Traits\ViajeTrait;

class ReservaController extends Controller
{

    use ViajeTrait;

    public function index(){

        $idUsu= auth()->id();
        //$reservas=Reserva::all()->where('cliente_id','=',$idUsu);
        $reservas=Reserva::all()->where('cliente_id', $idUsu);
        //$reservas=Reserva::paginate(9)->where('cliente_id', $idUsu);
        return view('reservas.index', ["reservas"=>$reservas]);
    }

    public function show(Reserva $reserva){

        $res=Reserva::findOrFail($reserva->id);
        return view('reservas.show', ["reserva"=>$res]);
    }

    public function edit(Reserva $reserva){

        $reserva=Reserva::findOrFail($reserva->id);
        $hoteles=Estancia::all();
        //$users = User::where("estado","=",1)->select("id","username") Para que sólo me duevuelva unos campos
        //return $hoteles;
        $transportes=Transporte::all();
        return view('reservas.edit', compact('reserva', 'hoteles', 'transportes'));
    }

    public function update(Request $request, Reserva $reserva){

       $fechaActual=date("d-m-Y");
       // $request->validate([
        //     'hotel'=>'required',
        //     'trasporte'=>'required',
        //     'fechaEntrada'=>'required|date|after_or_equal:'.$fechaActual,
        //     'fechaSalida'=>'required|date|after_or_equal:'.$fechaActual,
        // ]);

        $reserva->estancia_id=$request->hotel;
        $reserva->transporte_id=$request->transporte;
        $reserva->fechaReserva=$request->fechaEntrada;
        $reserva->fechaFinReserva=$request->fechaSalida;

        $reserva->save();

        return redirect()->route('reservas.show', $reserva)->with('mensaje', "Reserva actualizada con éxito");


    }

    public function create(Lugar $viaje){
        //return $viaje;
        //$viaje=Lugar::findOrFail($viaje->id);
        $hoteles=Estancia::all();
        $transportes=Transporte::all();
        // $transportes=Transporte::all('nombre');    Esto sólo devuelve esa columna
        return view('reservas.create', compact('viaje', 'hoteles', 'transportes'));
    }

    public function store(Request $request){
        //return $request->all();
        $reserva=new Reserva();
        $idUsu= auth()->id();
        //return $idUsu;
        $fechaActual=date("d-m-Y");
        // $request->validate([
        //     'hotel'=>'max:255',
        //     'trasporte'=>'max:255',
        //     'fechaEntrada'=>'required|date|after_or_equal:'.$fechaActual,
        //     'fecahSalida'=>'required|date|after_or_equal:'.$fechaActual,
        // ]);
        //return $request->hotel;
        $reserva->cliente_id= $idUsu;
        $reserva->estancia_id=$request->hotel;
        $reserva->transporte_id=$request->transporte;
        $reserva->fechaReserva=$request->fechaEntrada;
        $reserva->fechaFinReserva=$request->fechaSalida;
        $reserva->lugar_id=$request->destino;

        $reserva->save();

        return redirect()->route('reservas.show', $reserva)->with('mensaje', "Reserva añadida con éxito");

    }

    public function destroy(Reserva $r){
        // return $r;
        // if($r->delete())
        //     return 'bien';
        // else
        //     return 'mal';
        if($r->delete()){
            return redirect()->route('reservas.index')->with('mensaje', 'Reserva anulada correctamente');
        }else{
            return redirect()->route('reservas.index')->with('mensaje', 'No se ha podido cancelar la reserva');
        }
    }

}
