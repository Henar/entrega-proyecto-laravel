<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-danger">
  <div class="container-fluid">
  <a class="navbar-brand" href="{{url('/')}}">Viajes</a>
  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav me-auto mb-2 mb-lg-0">

      <li class="nav-item">
        <a href="{{route('viajes.index')}}" class="nav-link {{ request()->routeIs('viajes.*') && !request()->routeIs('viajes.create')? ' active' : ''}}">Listado de destinos</a>
      </li>

      @if(Auth::check())
        
      <li class="nav-item">
          <a href="{{route('viajes.create')}}" class="nav-link {{ request()->routeIs('viajes.create')? ' active' : ''}}">Añadir un nuevo destino</a>
      </li>

      <li class="nav-item">
        <a href="{{route('reservas.index')}}" class="nav-link {{ request()->routeIs('reservas.index')? ' active' : ''}}">Mostrar mis reservas</a>
    </li>

      @endif 

      <li class="nav-item">
        <a href="{{route('estancias.index')}}" class="nav-link {{ request()->routeIs('estancias.index') ? ' active' : ''}}">Listado de las distintas estancias</a>
      </li>

    </ul>
    
    @if(Auth::check() )
        {{-- <form class="d-flex">
          <input id="busqueda" class="form-control mr-sm-3" type="text" placeholder="Buscar" aria-label="Buscar">
        </form> --}}
        
        <ul class="navbar-nav navbar-right">
          <li class="nav-item font-weight-bold">
            <a href="{{ route('profile.show') }}"  class="nav-link h5">
               {{auth()->user()->nombre}}
            </a>
          </li>
            <li class="nav-item">
                <a href="{{ route('logout') }}"  class="nav-link"
                  onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();" >
                    <span class="glyphicon glyphicon-off"></span>
                    Cerrar sesión
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    @else
        <ul class="navbar-nav navbar-right">
          <li class="nav-item">
            <a href="{{ route('register') }}" class="nav-link">Registrarse</a>
          </li>
            <li class="nav-item">
              <a href="{{url('login')}}" class="nav-link">Login</a>
            </li>
        </ul>
    @endif 
  </div>
</div>
</nav>




