<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" >
    <link href="{{ url('/assets/css/estilo.css') }}" rel="stylesheet" >

    <title>@yield('titulo')</title>
  </head>
  <body>
    
    <script src="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    @include('layouts.partials.navbar')
    
    <div class="container-fluid">
        @yield('contenido')
    </div>
    
  </body>
</html>