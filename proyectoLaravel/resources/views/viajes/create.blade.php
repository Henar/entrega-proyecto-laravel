@extends('layouts.master')

@section('titulo')
    Viajes
@endsection

@section('contenido')
    
    <div class="row">
        <div class="offset-md-3 col-md-6">
            
            <div class="card">
                <div class="card-header text-center">
                    Añadir destino
                </div>
                
                <div class="card-body" style="padding:30px">
                    <form action="{{ route('viajes.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="pais">Introduzca el país de la ciudad que quiera añadir</label>

                            @error('pais')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror

                            <input type="text" name="pais" id="pais" class="form-control" value="{{old('pais')}}" required>
                        </div>
                        <div class="form-group">
                            <label for="ciudad">Introduzca la ciudad que quiera añadir</label><br>
                            
                            @error('ciudad')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            
                            <input type="text" name="ciudad" id="ciudad" class="form-control" value="{{old('ciudad')}}" required>
                        </div>
                        <div class="form-group">
                            <label for="precio">Introduzca el precio que cuesta la ciudad</label><br>
                            
                            @error('precio')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            
                            <input type="number" name="precio" id="precio" class="form-control" step="0.01" value="{{old('precio')}}" required>
                        </div>
                        <div class="form-group">
                            <label for="descripcion">Descripción</label>
                            
                            @error('descripcion')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            
                            <textarea name="descripcion" id="descripcion" class="form-control" rows="3">{{old('descripcion')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="imagen">Introduzca la foto del destino</label><br>
                             
                            @error('imagen')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror

                            <input type="file" name="imagen" id="imagen"/>
                        </div>
                        <br>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Añadir destino</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection