@extends('layouts.master')

@section('titulo')
    Viajes
@endsection

@section('contenido')
    @if (session ('mensaje'))
        <div class="alert alert-info">{{session('mensaje')}}</div>
    @endif
     
    <form method="POST" action="{{ route('viajes.destroy', $viaje)}}">
        @csrf
        @method('delete')
        <h1 style="margin-left: 25%">{{$viaje->pais}} ({{$viaje->ciudad}})</h1>
        <div class="row">
            <br>
            <div class="col-sm-3">
                <img src="{{asset('assets/imagenes/')}}/{{$viaje->imagen}}" style="width:406px"/>
            </div>
            <div class="col-sm-9">
                <h3>Lugar:</h3>
                <h5>{{$viaje->ciudad}} ({{$viaje->pais}})</h5>

                <h3>Precio:</h3>
                <h5>{{$viaje->precio}}€</h5>

                
                <h3>Descripción:</h3>
                
                @php
                    $parrafos=explode(PHP_EOL, $viaje->descripcion);
                @endphp
                @foreach ($parrafos as $parrafo)
                    <p>{{$parrafo}}</p>
                @endforeach

               

                <br/>
                <a class="btn btn-warning" name="editar" style="margin-right: 7px" href = '{{ route('viajes.edit', $viaje)}}'>Editar</a>
                <a class="btn btn-light btn-outline-dark" name="volver" href = '{{ route('viajes.index')}}' style="margin-left: 7px">Volver al listado</a>
                {{-- @php
                    $reserva="lugar=".$viaje;
                @endphp --}}
                <a class="btn btn-success" name="revision" style="margin-right: 7px" href = '{{ route('reservas.create', $viaje)}}'>Hacer Reserva</a>
                {{-- <h4>{{$viaje->pais}}</h4> --}}
                <button class="btn btn-danger" name="borrar" style="margin-right: 7px" type="submit">Borrar destino</button>
            </div>
        </div>
    </form>
@endsection