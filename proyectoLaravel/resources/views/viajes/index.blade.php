@extends('layouts.master')

@section('titulo')
    Viajes
@endsection

@section('contenido')
    
    <form method="POST">
        @csrf
        <h3>Mostrando todos los posibles destinos</h3>
        <div class="row">
            @foreach($viajes as $viaje)
            <div class="col-xs-12 col-sm-6 col-md-4 ">
                <br>
                <a href="{{ route('viajes.show', $viaje)}}">                 
                        <h4 style="min-height:45px;margin:5px 0 10px 0">{{$viaje->ciudad}} ({{$viaje->pais}})</h4>
                        <img src="{{asset('assets/imagenes/')}}/{{$viaje->imagen}}" style="width:406px;margin-bottom:20px;padding:7px;"
                            class="rounded border border-3"/>
                        
                    </a>
                </div>
            @endforeach
        </div>
        <br>
        {{$viajes->links()}}
    </form>
    
@endsection