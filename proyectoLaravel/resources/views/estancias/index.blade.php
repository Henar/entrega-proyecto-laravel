@extends('layouts.master')

@section('titulo')
    Estancias
@endsection

@section('contenido')
    
    <form method="POST">
        @csrf
        <h3>Mostrando todas las posibles estancias</h3>
        <div class="row">
            @foreach($estancias as $estancia)
            <div class="col-xs-12 col-sm-6 col-md-4 ">
                <br>
                <a href="{{ route('estancias.show', $estancia)}}">                 
                        <h4 style="min-height:45px;margin:5px 0 10px 0">{{$estancia->hotel}}</h4>
                        <img src="{{asset('assets/imagenes/')}}/{{$estancia->imagen}}" style="height:325px;margin-bottom:20px;padding:7px;"
                            class="rounded border border-3"/>
                        
                    </a>
                </div>
            @endforeach
        </div>
        <br>
        {{$estancias->links()}}
    </form>
    
@endsection