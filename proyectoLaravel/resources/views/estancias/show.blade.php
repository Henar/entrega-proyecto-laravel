@extends('layouts.master')

@section('titulo')
    Estancias
@endsection

@section('contenido')
    
    <form method="POST" action="{{ route('estancias.destroy', $estancia)}}">
        @csrf
        @method('delete')
        <h3>Mostrando todas las posibles estancias</h3>
        <div class="row">
           
            <div class="col-xs-12 col-sm-6 col-md-4 ">
                <br>               
                    <h4 style="min-height:45px;margin:5px 0 10px 0">{{$estancia->hotel}}</h4>
                    <img src="{{asset('assets/imagenes/')}}/{{$estancia->imagen}}" style="height:360px;margin-bottom:20px;padding:7px;"
                        class="rounded border border-3"/>
                        <br><br>
                    <button class="btn btn-danger" name="borrar" style="margin-right: 7px" type="submit">Borrar estancia</button>
                </div>
           
        </div>
        <br>
       
    </form>
    
@endsection