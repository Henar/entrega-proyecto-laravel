@extends('layouts.master')

@section('titulo')
    Reserva
@endsection

@section('contenido')
    @if (session ('mensaje'))
        <div class="alert alert-info">{{session('mensaje')}}</div>
    @endif
    @php
        $lugar=$reserva->lugar;
        $estancia=$reserva->estancia;
        $transporte=$reserva->transporte;
        $fechaEntr=$estancia->fechaEntrada;
        $fechaSal=date("d-m-Y", strtotime($estancia->fechaSalida));
        $fechaEntr=date("d-m-Y", strtotime($fechaEntr));
    @endphp
     
    <form method="POST" action="{{ route('reservas.destroy', $reserva)}}">
        @csrf
        @method('delete')
        {{-- <p>{{$reserva}}</p> --}}
        <h1 style="margin-left: 25%">{{$lugar->pais}} ({{$lugar->ciudad}})</h1>
        <div class="row">
            <br>
            <div class="col-sm-3">
                <img src="{{asset('assets/imagenes/')}}/{{$lugar->imagen}}" style="height:25%"/>
            </div>
            <div class="col-sm-9">

                <h3>Hospedaje:</h3>
                <h5>{{$estancia->hotel}}</h5>

                <h3>Medio de Transporte elegido:</h3>
                <h5>{{$transporte->nombre}}</h5>

                <h3>Precio:</h3>
                <h5>{{$lugar->precio}}€</h5>

                <br>
                <h3>Descripción de la ciudad:</h3>
                
                @php
                    $parrafos=explode(PHP_EOL, $lugar->descripcion);
                @endphp
                @foreach ($parrafos as $parrafo)
                    <p>{{$parrafo}}</p>
                @endforeach

                <br>
                <h3>Fecha de entrada al hotel</h3>
                <h5>{{$fechaEntr}}</h5>
               
                <br>
                <h3>Fecha de salida del hotel</h3>
                <h5>{{$fechaSal}}</h5>

                <br/>
                <a class="btn btn-warning" name="editar" style="margin-right: 7px" href = '{{ route('reservas.edit', $reserva)}}'>Editar</a>
                <a class="btn btn-light btn-outline-dark" name="volver" href = '{{ route('reservas.index')}}' style="margin-left: 7px">Volver a las reservas</a>
                <button class="btn btn-danger" name="borrar" style="margin-right: 7px" type="submit">Borrar reserva</button>
            </div>
        </div>
    </form>
@endsection