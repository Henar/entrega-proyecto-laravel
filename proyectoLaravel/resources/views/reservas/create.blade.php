@extends('layouts.master')

@section('titulo')
    Hacer Reserva
@endsection

@section('contenido')
    @php
        $fechaActual=date('Y-m-d');
    @endphp

    @php
        $url=$_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI'];
        $resultado=parse_url($url, PHP_URL_PATH);
        //echo $resultado;
        // echo $_GET['lugar'];
        $separado=explode('/', $resultado);
        $lugar=$separado[count($separado)-1];
        //echo $lugar;
        $viaje=DB::table('lugares')->where('slug', $lugar)->get();
        //var_dump ($viaje);
        //echo $viaje;
        $separacionViaje=explode(',', $viaje);
        //echo $separacionViaje[0];
        //echo '<br/>'. $viaje[0];
        $valores=array();
        // echo '<br/>';
        //print_r($separacionViaje);
        //echo ($separacionViaje[1]);
        for ($i=0; $i <count($separacionViaje)-2 ; $i++) { 
            $separaElementos=explode(':', $separacionViaje[$i]);
            //echo $separaElementos[1];
            //print_r($separaElementos);
            if(count($separaElementos)>1){
                // echo $separaElementos[1];
            // echo '<br/>';
                if($i==0){
                    $id=substr($separaElementos[1],2);
                // echo $id;
                    $valores[$separaElementos[0]]=$id;
                }

                $valores[$separaElementos[0]]=$separaElementos[1];
            }else{
                $valores['"descripcion"'].=$separaElementos[0];
            }
        }
        //print_r($valores);
        // echo count($valores);
        // echo $valores['[{"id"'];
        // foreach ($valores as $key=>$value) {
        //     echo 'valor: '. $value. '  clave: '. $key;
        //         echo '<br/>';
        // }

    @endphp

    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">
                    {{-- <h4>{{$viaje}}</h4> --}}
                    Crear la Reserva para <b>{{$valores['"ciudad"']}} ({{$valores['"pais"']}})</b>
                </div>
                <div class="card-body" style="padding:30px">
                    <form action="{{ route('reservas.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <br>
                        <input name="destino" type="hidden" value="{{$valores['[{"id"']}}">
                        {{-- <div class="col-sm-3">
                            <img src="{{asset('assets/imagenes/')}}/{{$valores['"imagen"']}}" style="height:100%"/>
                        </div> --}}

                        <div class="form-group">
                            <h4>Precio del viaje: {{$valores['"precio"']}}</h4>
                        </div>

                        <div class="form-group">
                            <label for="hotel">Introduzca el lugar donde se quiera hospedar</label><br>
                            
                            @error('hotel')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            {{-- <p>{{$hoteles[0]->id}}</p> --}}
                            {{-- <p>{{$hoteles[0]}}</p> --}}
                            <select name="hotel" id="hotel" class="form-control" required>
                                @foreach ($hoteles as $hotel)
                                    <option value="{{$hotel->id}}">{{$hotel->hotel}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="fechaEntrada">Fecha Entrada al Hotel</label>
                            
                            @error('fechaEntrada')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            
                            <input type="date" name="fechaEntrada" id="fechaEntrada" class="form-control" min="{{$fechaActual}}" value="{{old('fechaEntrada')}}" required>
                        </div>
                        <div class="form-group">
                            <label for="fechaSalida">Fecha Salida del Hotel</label>
                            
                            @error('fechaSalida')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror

                            <script>
                                $(document).ready(function(){
                                    $("#fechaSalida").click(function(){
                                        let valorFecha=document.getElementById('fechaEntrada').value;
                                        let etiqueta=document.getElementById('fechaSalida');
                                        etiqueta.setAttribute("min", valorFecha);
                                    });
                                });
                            </script>
                            
                            <input type="date" name="fechaSalida" id="fechaSalida" class="form-control" value="{{old('fechaSalida')}}" required>
                        </div> 

                        <div class="form-group">
                            <label for="transporte">Introduzca el medio de transporte con el que quiera viajar</label><br>
                            
                            @error('transporte')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            {{-- <p>{{$transportes[0]}}</p> --}}
                            <select name="transporte" id="transporte" class="form-control" required>
                                @foreach ($transportes as $transporte)
                                   <option value="{{$transporte->id}}">{{$transporte->nombre}}</option>
                                @endforeach
                            </select>
                            
                        </div>
                        <br>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Añadir reserva</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection