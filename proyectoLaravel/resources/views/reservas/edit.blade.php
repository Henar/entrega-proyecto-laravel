@extends('layouts.master')

@section('titulo')
    Reserva
@endsection

@section('contenido')
    @php
        $fechaActual=date('Y-m-d');
    @endphp

    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">
                    Modificar la Reserva de <b>{{$reserva->lugar->ciudad}} ({{$reserva->lugar->pais}})</b>
                </div>
                <div class="card-body" style="padding:30px">
                    <form action="{{ route('reservas.update', $reserva)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')

                        <div class="form-group">
                            <label for="hotel">Introduzca el lugar donde se quiera hospedar</label><br>
                            
                            @error('hotel')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            {{-- <p>{{$hoteles[0]->id}}</p> --}}
                            <select name="hotel" id="hotel" class="form-control" required>
                                <option value="{{old($reserva->estancia->hotel)}}" selected>{{$reserva->estancia->hotel}}</option>
                                @foreach ($hoteles as $hotel)
                                    <h4>{{$hotel->hotel}}</h4>
                                    <option value="{{$hotel->id}}">{{$hotel->hotel}}</option>
                                @endforeach
                            </select>
                            
                        </div>
                        <div class="form-group">
                            <label for="fechaEntrada">Fecha Entrada al Hotel</label>
                            
                            @error('fechaEntrada')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            
                            <input type="date" name="fechaEntrada" id="fechaEntrada" class="form-control" min="{{$fechaActual}}" value="{{old('fechaEntrada',  $reserva->estancia->fechaEntrada)}}" required>
                        </div>
                        <div class="form-group">
                            <label for="fechaSalida">Fecha Salida del Hotel</label>
                            
                            @error('fechaSalida')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror

                            <script>
                                $(document).ready(function(){
                                    $("#fechaSalida").click(function(){
                                        let valorFecha=document.getElementById('fechaEntrada').value;
                                        let etiqueta=document.getElementById('fechaSalida');
                                        etiqueta.setAttribute("min", valorFecha);
                                    });
                                });
                            </script>
                            
                            <input type="date" name="fechaSalida" id="fechaSalida" class="form-control" value="{{old('fechaSalida', $reserva->estancia->fechaSalida)}}" required>
                        </div> 

                        <div class="form-group">
                            <label for="transporte">Introduzca el medio de transporte con el que quiera viajar</label><br>
                            
                            @error('transporte')
                                <br>
                                <small>*{{$message}}</small>
                                <br>
                            @enderror
                            <select name="transporte" id="transporte" class="form-control" required>
                                <option value="{{$reserva->transporte->id}}" selected>{{$reserva->transporte->nombre}}</option>
                                @foreach ($transportes as $transporte)
                                   <option value="{{$transporte->id}}">{{$transporte->nombre}}</option>
                                @endforeach
                            </select>
                            
                        </div>
                        <br>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Modificar destino</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection