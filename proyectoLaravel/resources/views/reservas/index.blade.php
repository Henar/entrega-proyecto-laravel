@extends('layouts.master')

@section('titulo')
    Reservas
@endsection

@section('contenido')

    @if (session ('mensaje'))
        <div class="alert alert-info">{{session('mensaje')}}</div>
    @endif
    
    <form method="POST">
        @csrf
        {{-- @php
            require_once ('../../../../App/Models/Lugar.php');
        @endphp --}}
        
        @if(empty($reservas))
            <h3>Todavía no ha realizado ninguna reserva</h3>
        @else
            <h3>Mostrando todas las reservas de {{Auth::user()->nombre}}</h3>
            <div class="row">
                    
                    @foreach($reservas as $reserva)
                        {{-- @php
                            $lugarDestino=Lugar::all()->where('id', $reserva['destinoLugar_id']);
                        @endphp --}}
                        
                        <div class="col-xs-12 col-sm-6 col-md-4 ">
                            <br>
                            <a href="{{ route('reservas.show', $reserva)}}">  
                                {{-- <h4>{{$reserva}}</h4> --}}
                                {{-- <h4>{{$reserva['destinoLugar_id']}}</h4>  --}}        
                                {{-- <h4>{{$reserva->transporte['nombre']}}</h4>  --}}
                                {{-- <h4>{{$reserva->estancia['hotel']}}</h4> --}}
                                {{-- <h4>{{$reserva->lugar['ciudad']}}</h4> --}}
                                <h4>Lugar de destino de la reserva: {{$reserva->lugar['ciudad']}}</h4>  
                                <img src="{{asset('assets/imagenes/')}}/{{$reserva->estancia['imagen']}}" style="height:225px;margin-bottom:20px;padding:7px;"
                                class="rounded border border-3"/> 
                                    
                            </a>
                            <h5>Fecha del inicio de la reserva: {{date("d-m-Y", strtotime($reserva->fechaReserva))}}</h5>
                            <h5>Fecha en la que acaba la reserva: {{date("d-m-Y", strtotime($reserva->fechaFinReserva))}}</h5>
                        </div>
                    @endforeach
            </div>
        @endif
        <br>
        {{-- {{$reservas->links()}} --}}
    </form>
    
@endsection